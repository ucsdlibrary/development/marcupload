# marcupload
A legacy tool for AA/marcupload.

## Deploying with GitLab ChatOps
This application is setup to deploy via GitLab's chatops feature.

Environments to deploy:
- staging
`/gitlab ucsdlibrary/development/marcupload run staging_deploy`

- production
`/gitlab ucsdlibrary/development/marcupload run production_deploy`

By default, it will deploy the specified default branch, which is
`master`. If you would like to specify a different branch or tag, you can pass
that as an argument to the slack command.

Example: `/gitlab ucsdlibrary/development/marcupload run production_deploy 2.7.0`

This will deploy the `2.7.0` tag to `production`.

### Scheduling A Production Deployment
The application also supports setting up a GitLab [Scheduled
Pipeline][gitlab-schedule] to support production deployments during the
Operations service window (6-8am PST).

An example schedule using [Cron syntax][cron] might be `30 06 * * 1` which would deploy
the application at 06:30AM on Monday.

Make sure you set the `Cron timezone` in the schedule for `Pacific Time (US and
Canada)`

After the deploy, make sure you de-active or delete the Schedule.
Otherwise the application may accidentally deploy again the following week.

[cron]:https://en.wikipedia.org/wiki/Cron
[gitlab-schedule]:https://docs.gitlab.com/ee/ci/pipelines/schedules.html

