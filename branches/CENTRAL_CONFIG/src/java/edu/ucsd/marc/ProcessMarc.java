package edu.ucsd.marc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.jsp.JspWriter;

import org.marc4j.MarcReader;
import org.marc4j.helpers.DefaultHandler;
import org.marc4j.marc.*;
import org.marc4j.helpers.ErrorHandlerImpl;


/*
import com.bpeters.james.MarcReader;
import com.bpeters.james.MarcReaderException;
import com.bpeters.james.helpers.DefaultHandler;
import com.bpeters.marc.*;
import com.bpeters.util.ErrorHandlerImpl;
*/

import edu.ucsd.marc.util.MarcProperties;
//import edu.ucsd.marc.util.MarcMail;

/** Marc is utility to handel the transformation process calls.
  * This uses the James.jar file.
  * 
  * To Use:
  * ProcessMarc processMarc = new ProcessMarc();
  * processMarc.process(String[] args);
  * 
  * Steps of process:
  * process():
  * 1. Do error checking.
  * 2. Read in property file
  * 3. set local vars, like CAMPUSES as read from property file
  * 4. Process Marc file and create new Marc files for each university.
  * 
  */
public class ProcessMarc extends DefaultHandler{
    private boolean debug = false;

    public static String PROP_PATH = "";	// default properties path for MarcProperties (local file)
	
	// local objects
    private Record record;
    private String MARC_FILE;
    private String PROPERTY_FILE_NAME;
    private Object[] CAMPUSES;

    private ArrayList outputStreams;	// for each university, we have a fileStream
    
    private int bit;
    protected static DataField datafield;

    public void startRecord(Leader leader) {
    	if(debug) {
			System.err.println("Starting new record ==================");
    	}

	    this.record = new Record();				// create a new instance of a Record object
	    this.bit = 0;
//	    record.setLeader(leader);				// register the Leader object
        record.add(leader);	// CHANGED BY KELLEN FOR MARC4J
    }

    public void controlField(String tag, char[] data) {
	    // add a new control field to the record object
		if(debug) {
			System.err.println("Adding control field " + tag + " with data " + (new String(data)));
			/* only log these when we're debugging -- otherwise the error throw to screen. */
			try {
				record.add(new ControlField(tag, data));
			} catch (Exception e) {
			    System.err.println("Error adding control field " + tag + " with data " + (new String(data)));
			}
	    } else {
			// skip 001 if we already have one
			boolean process = true;
			if ( tag.equals("001") && record.hasControlNumberField() )
			{
				process = false;
			}

			if ( process )
			{
				record.add(new ControlField(tag, data));
			}
		}
		
    }

    public void startDataField(String tag, char ind1, char ind2) {
	    // create a new data field
	    datafield = new DataField(tag, ind1, ind2);
    }

    public void subfield(char identifier, char[] data) {
	    // register a new data element to the current data field
	    datafield.add(new Subfield(identifier, data));
	    
	    if(debug) {
	    	if(datafield.getTag().startsWith("2")) {
	    		System.err.println("identifier " + identifier + " with data " + (new String(data)));
	    	}
	    }
	    
	    if(datafield.getTag().equals("920")){
			for (int i = 0; i < this.CAMPUSES.length; i++) {
		    	if((new String(data)).equals((String)this.CAMPUSES[i])){
		    		bit = bit | (new Double(Math.pow(2,i))).intValue();
		    		break;
		    	}
		    }
	    }
	}

    public void endDataField(String tag) {
	    record.add(datafield);			// add a new data field to the record object
    }

    public void endRecord() {
		
		for (int i = 0; i < outputStreams.size(); i++) {		// for each university, create a new file to write to
	    	OutputStreamWriter osw = (OutputStreamWriter)outputStreams.get(i);	// same number of printWriters as universities
			if((bit & (new Double(Math.pow(2,i))).intValue()) > 0){
				try{						// serialize the Record object to tape format
					osw.write(record.marshal());	// and write tape format record to standard output
				}catch (MarcException e){
					e.printStackTrace();
				}catch (IOException e) {
					e.printStackTrace();
				}
			}
	    }
    }

    /** @param String infile - marc record file to read
      * @param String[] universities - list of universities to keep track of
      */
    private String[] print(String savePath, String sourcePath, String _file, JspWriter out) throws IOException, Exception {
      // init
      outputStreams = new ArrayList();
      String outputfilename;
      String[] outFiles = new String[this.CAMPUSES.length];

      try {
        // create sub directory
        String dirname = _file.substring(0,_file.indexOf(MarcProperties.getProperty(MarcProperties.COMMON_MARC_EXT)));
        savePath = savePath + dirname;
        File f = new File(savePath);

        // only create directory if it does not already exist
        if(!f.exists()) {
          if(!f.mkdir()) {
            throw new Exception("Failed to create dir: " + f.getPath());
          }
        }

        // check to make sure this is actually a directory (and not a file in its place)
        if(!f.isDirectory()) {
            throw new Exception(f.getPath() + " is not a valid save path.");
        }
	 
        // for each university, create a new file to write to
        for (int i = 0; i < this.CAMPUSES.length; i++) {
          outputfilename = new String((String)this.CAMPUSES[i] + "_" + _file);
          outFiles[i] = outputfilename;
          FileOutputStream fos = new FileOutputStream(new File(savePath, outputfilename));
          OutputStreamWriter w = new OutputStreamWriter(fos, "ISO8859_1");
          outputStreams.add(w);
        }

        ProcessMarc pm = new ProcessMarc();
        pm.setOutputStreamWriters(this.outputStreams);
        pm.setCAMPUSES(getCAMPUSES());
        MarcReader marcReader = new MarcReader();		// Create a new MarcReader instance.
        marcReader.setMarcHandler(pm);				// Register the MarcHandler implementation.
        marcReader.setErrorHandler(new ErrorHandlerImpl());	// Register the MarcHandler implementation.
        marcReader.parse(sourcePath + _file);			// Send the file to the parse method.

        // close
        for (int i = 0; i < this.outputStreams.size(); i++) {
          OutputStreamWriter osw = (OutputStreamWriter)outputStreams.get(i);
          osw.close();
        }
      } catch (IOException e) {
        e.printStackTrace();
      }

      return outFiles;
    }
    
	/** This handles the order of processing.
	  */
	public String[] process(String saveDir, String sourceDir, String fileName, String propertiesPath, JspWriter out) throws Exception {
                this.PROP_PATH = propertiesPath;
		this.PROPERTY_FILE_NAME = MarcBean.DEFAULT_PROPERTY_FILE_NAME;
		errorCheck(saveDir, sourceDir, fileName);						// do error checking
		setCAMPUSES(MarcProperties.getArrayProperty(MarcBean.CAMPUSES, false));
		return print(saveDir, sourceDir, this.MARC_FILE, out);
	}

	/** This performs error checking and sets flags while looping through
	  */
	private void errorCheck(String saveDir, String sourceDir, String fileName) throws Exception{
          this.MARC_FILE = null;
          File f = new File(sourceDir, fileName);
          if(!f.exists())
            throw new Exception("Source file " + sourceDir + fileName + " not found.");
          this.MARC_FILE = f.getName();
          // check for existance of MARC_FILE
          if(this.MARC_FILE == null)
            throw new Exception("MARC FILE does not exist.");
	}

	// mutators
    public void setOutputStreamWriters(ArrayList _printWriters){
    	this.outputStreams = _printWriters;
    }
    
    public void setCAMPUSES(Object[] _CAMPUSES){
    	this.CAMPUSES = _CAMPUSES;
    }

	// accessors
	public Object[] getCAMPUSES(){
		return this.CAMPUSES;
	}
	
	public String getMARC_FILE(){
		return this.MARC_FILE;
	}

}
