<%@ page import="java.io.*,java.util.*"%>
<%
	try
	{
		// need to write output files
		String basePath = application.getInitParameter("base-path");
		File destDir = new File(basePath);

		// need to write source files
		File base = new File(getServletContext().getRealPath("/" + request.getServletPath()));
		File parentDir = base.getParentFile();
		File rawDir = new File( parentDir, "raw" );
		if ( ! rawDir.exists() )
		{
			rawDir.mkdir();
		}

		// need to read props file
		Properties props = new Properties();
		props.load(
			new FileInputStream( new File(parentDir, "marc.properties") )
		);

		if ( destDir.isDirectory() && destDir.canWrite() && rawDir.isDirectory() && rawDir.canWrite() && props.size() > 0 )
		{
			%>SUCCESS<%
		}
		else
		{
			%>FAIL: destDirisDirectory: <%=destDir.isDirectory()%>, destDir.canWrite: <%=destDir.canWrite()%>, rawDir.isDirectory(): <%=rawDir.isDirectory()%>, rawDir.canWrite(): <%=rawDir.canWrite()%>, props.size: <%=props.size()%><%
		}
	}
	catch ( Exception ex )
	{
		%>FAIL-EXCEPTION: <%=ex.toString()%><%
	}
%>
