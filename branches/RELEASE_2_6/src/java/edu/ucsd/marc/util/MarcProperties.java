package edu.ucsd.marc.util;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import edu.ucsd.marc.ProcessMarc;

/**
  */
public class MarcProperties {
    // public vars
    public  static final String COMMON_MARC_EXT = "COMMON_MARC_EXT";
    public  static final String CAMPUSES        = "CAMPUSES";
    private static final String DELIMETER       = "DELIMETER";
    
    // Define the hash table for the necessary properties
    private HashMap properties = new HashMap(32,0.8f);
    
    // This is the name of the run time properties.  This file will be at the
    // root of the web/command.
    private static final String DEFAULT_PROP_PATH = ProcessMarc.PROP_PATH + "marc.properties";
    private static String propertiesPath = DEFAULT_PROP_PATH;
    private static MarcProperties ap = new MarcProperties();       // Init myself

	/** Looks into the hashtable for the pName value
      *
      * @param pName hashtable key for retrieving a Property value
      * @return Either a string value or null if not found
      *
      */
    private String retrieveProperty(String pName)
    throws ClassCastException{
		return (String)properties.get(pName);
    }

	/** Looks into the hashtable for the pName value
      *
      * @param pName hashtable key for retrieving a Property value
      * @return Either a string value or null if not found
      *
      */
    private Object[] retrieveArrayProperty(String pName)
    throws ClassCastException{
		return (Object[])properties.get(pName);
    }

    /** This is used to allow the programmer the option of forcing a re-read
      * of the property file. The default will be false.
      * @param pName - the name of the property value to read
      * @param re_read - true means force the re-read of the props file
      * @return - the value of the property
      */
    public static String getProperty(String pName)
    throws ClassCastException{
		return getProperty(pName,false);
    }
    
    /** This is used to allow the programmer the option of forcing a re-read
      * of the property file. The default will be false.
      * @param pName - the name of the property value to read
      * @param re_read - true means force the re-read of the props file
      * @return - the value of the property
      */
    public static String getProperty(String pName,boolean re_read)
    throws ClassCastException{
		if(ap == null || re_read) {
		    ap = new MarcProperties();
		}
		return ap.retrieveProperty(pName);
    }
    
    public static Object[] getArrayProperty(String pName, boolean re_read)
    throws ClassCastException{
		if(ap == null || re_read) {
		    ap = new MarcProperties();
		}
		return ap.retrieveArrayProperty(pName);
    }
    
    /** Private Constructor.  This will load all the properties in the hash table.
      */
    private MarcProperties(){
		try {
		    // This will contruct a new buffer reader that will hold the information from the file.
		    BufferedReader br = new BufferedReader(new FileReader(propertiesPath));  // Slurp the file
		    String s = br.readLine();
		    String key = null;
		    String value = null;
		    boolean slashEnded = false;
		    int p = -1;
		    while(s != null) {                                     	// Parse the file string
				p = s.indexOf('=');
				if(slashEnded){
					value = value.substring(0,value.length() - 1) + "\n" + s;
					properties.put(key, value);
					slashEnded = value.endsWith("\\");
				}else if(p != -1 && !s.startsWith("#")){			// do not read in comments
				    key = s.substring(0,p);
				    value = s.substring(p+1);
				    if(properties.get(DELIMETER) != null 			// the user wants this to be an array
				    	&& value.indexOf((String)properties.get(DELIMETER)) > -1)						// make this String[]
				    	properties.put(key,convertToArray(value, (String)properties.get(DELIMETER)));
				    else
				    	properties.put(key,value);
				    slashEnded = value.endsWith("\\");				// means this has multiple lines
				}
				s = br.readLine();
		    }
		}catch(FileNotFoundException fnf) {
		    System.err.println("Unable to find the file: " + this.propertiesPath + " in the server root directory");
		    fnf.printStackTrace();
		}catch (IOException e){
		    System.err.println("Unable to read the file: " + this.propertiesPath + " in the server root directory");
		    e.printStackTrace();
		}	
    }
    
    /** This will take a delimited String and convert it to
      * a String[] using StringTokenizer and ArrayList
      * @param String _value - the value to convert to String[]
      * @param String _delimeter - the delimiter
      * @return String[] - the value converted to String[]
      */
    private Object[] convertToArray(String _value, String _delimiter){
    	ArrayList a = new ArrayList();
    	StringTokenizer s = new StringTokenizer(_value, _delimiter);
    	while(s.hasMoreTokens()){
    		a.add(s.nextToken());
    	}
    	return a.toArray();
    }
}
