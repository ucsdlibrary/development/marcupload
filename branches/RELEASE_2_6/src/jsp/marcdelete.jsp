<%
 /* marcdelete.jsp
  * 
  * deletes directories and their files through a web form
  * step 1. check directories you would like to delete
  * step 2. confirm deletion on each directory
  * step 3. files are deleted
  *
  * should be placed where directories to be deleted are located
  * eg: in shared_cataloging for marc files
  */
%>
<%@ page import="java.util.*, java.io.*, javax.naming.*"%>
<jsp:useBean id="formBean" scope="page" class="edu.ucsd.library.util.http.FormProcessor" />

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Shared Cataloging Program - Delete Files</title>
<style type="text/css" media="all">
body {
	background: white;
	color: black;
	}

th {
	background: #D4E4F4;
	color: black;
	border: 1px solid;
	}

table {
	border: 1px dotted;
	}

input, textarea {
	font: 13px courier;
	}

.error {
	color: red;
	font-weight: bold;
	}
</style>
</head>
<body>

<%
File dir;
String sharedPath = (String)new InitialContext().lookup("java:comp/env/clusterSharedPath");
String path = sharedPath + application.getInitParameter("base-path"); // path where files are located
File [] files;
int i;

if (request.getMethod().equals("POST")) { 
  formBean.processForm(request, out);		// process form input

  /* get current files up for deletion */
  dir = new File(path);  	// File of current File's parent
  files = dir.listFiles();	// listing of Files in the current dir

  /* if "confirm" is set, delete the directories, otherwise show confirmation page */
  if(formBean.getFieldValue("confirm") != null) {
    /* cycle through directories in CWD (these are up for deletion) as long as they're not the web-inf dir */
    for(i=0; i < files.length; i++) {
      if(files[i].isDirectory() 
         && !(files[i].getName().compareTo("WEB-INF") == 0) 
         && !(files[i].getName().compareTo("web-inf") == 0) ) {
        /* if the name of this directory matches, delete this file */
        String val = formBean.getFieldValue(files[i].getName());
        if(val != null) {
          /* if deletion is confirmed, delete all files in the dir, then the dir itself */
          if(val.compareTo("delete") == 0) {
            out.println("<b>deleting:</b> " + files[i].getName() + "<br /><br /><b>deleted:</b><br />");
            File [] childFiles = files[i].listFiles();
            /* iterate through all the files in the dir and delete them one at a time */
            for(int k=0; k < childFiles.length; k++) {
              out.println(childFiles[k].getName() + "<br>");
              childFiles[k].delete();
            }
            files[i].delete();	// delete the directory
          }
        }
      }
    }
  } else {
%>
  <h1>Confirm deletion</h1>
  <p>Please <b>uncheck</b> any files that you <b>do not</b> want deleted, then click "confirm". </p>
  <form method="post" action="<% out.print(request.getContextPath() + request.getServletPath()); %>">
  <table>
  <th>delete?</th><th>filename</th>
<%
    /* cycle through directories in CWD (these are up for deletion) */
    for(i=0; i < files.length; i++) {
      if(files[i].isDirectory()
         && !(files[i].getName().compareTo("WEB-INF") == 0) 
         && !(files[i].getName().compareTo("web-inf") == 0)) {
        /* if the name of this directory matches, redisplay this file for confirmation */
        String val = formBean.getFieldValue(files[i].getName());
        if(val != null) {
          if(val.compareTo("delete") == 0) {
             out.print("<tr><td><input type=\"checkbox\" value=\"delete\" name=\"" + files[i].getName() + "\" checked /></td>");
             out.print("<td>" + files[i].getName() + "</td></tr>");
          }
        }
      }
    }
%>
  <tr><th colspan="2"><input type="submit" name="confirm" value="confirm" /></th></tr>
  </table>
  </form>
<%
  }
} else {
%>

<h1>Select Files</h1>
<p>Please select the files you would like deleted. This will delete <em>all</em> campus-specific files associated with this source file</p>
<form method="post" action="<% out.print(request.getContextPath() + request.getServletPath()); %>">
<table>
 <tr>
  <th>delete?</th><th>filename</th>
 </tr>
<%
  //look through all subdirectories (where the marc files are stored)
  dir = new File(path);
  files = dir.listFiles();	// listing of Files in the current dir

  /* iterate over each file, using those that are directories */
  for(int j=0; j< files.length; j++) {
    /* display directories only */
    if(files != null
       && files[j].isDirectory() 
       && !(files[j].getName().compareTo("WEB-INF") == 0) 
       && !(files[j].getName().compareTo("web-inf") == 0)) {
%>
 <tr>
  <td>
<%    out.print("<input type=\"checkbox\" value=\"delete\" name=\"" + files[j].getName() + "\" />"); %>
  </td>
  <td>
<%    out.print(files[j].getName() + "<br />"); %>
  </td>
 </tr>
<%
    }
  }
%>

</table>

<input type="submit" value="submit">
</form>

<%
} // end if request method = post
	
%>

</body>
</html>




