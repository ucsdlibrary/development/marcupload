<%@ page import="java.util.*, java.io.*, javax.naming.*"%>

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Shared Cataloging Program - List</title>
</head>
<body>
<h1>Debug: <%=request.getParameter("path")%></h1>
<%
	InitialContext ctx = new InitialContext();
	String sharedPath = (String)ctx.lookup("java:comp/env/clusterSharedPath");
	String baseDir = sharedPath + application.getInitParameter("base-path");
	String pathStr = request.getParameter("path");
	File path = null;
	if ( pathStr != null )
	{
		path = new File( baseDir, pathStr );
	}
	if ( path == null || !path.exists() || !path.isDirectory() )
	{
		path = new File( baseDir );
	}

	if ( path == null || !path.exists() || !path.isDirectory() )
	{
		%><p>Error: path does not exist: <%=request.getParameter("path")%></p><%
	}
	else
	{
		File[] contents = path.listFiles();
		for ( int i = 0; i < contents.length; i++ )
		{
			File f = contents[i];
			out.println("<li>");
			if ( f.isDirectory() )
			{
				out.print("<a href=\"debug.jsp?path="); 
				if (pathStr != null && !pathStr.equals("") )
				{
					out.print( pathStr + "/" );
				}
				out.print(f.getName() + "\">");
			}
			out.println( f.getName() );
			if ( f.isDirectory() )
			{
				out.println("</a>");
			}
			if ( f.isFile() )
			{
				out.print(" (" + f.length() + ")");
			}
			out.println( "</li>" );
		}
	}
%>
</body>
</html>
