<jsp:useBean id="marcBean" scope="page" class="edu.ucsd.marc.MarcBean" />
<jsp:useBean id="formBean" scope="page" class="edu.ucsd.library.util.http.FormProcessor" />
<%@ page import="java.io.*,javax.naming.*,java.util.*,edu.ucsd.marc.util.MarcProperties"%>
<%
/*
 * CONFIG SECTION
 */

/* web path to the shared_cataloging app */
String webPath = "/shared_cataloging/";
/* path to the shared_cataloging/processed dir relative to this app; no preceding slash */
String sharedPath = (String)new InitialContext().lookup("java:comp/env/clusterSharedPath");
String saveDir = sharedPath + application.getInitParameter("base-path"); // saveDir for marcBean
%>
<html>
<head><title>Marc upload</title></head>
<style type="text/css" media="screen">
body {
	background: white;
	color: black;
	}

th {
	background: #D4E4F4;
	color: black;
	border: 1px solid;
	}

table {
	border: 1px dotted;
	}

input, textarea {
	font: 13px courier;
	}

.error {
	color: red;
	font-weight: bold;
	}
</style>
<body>

<%

if (request.getMethod().equals("POST")) { 

  LinkedList extensions = new LinkedList();
  extensions.add(".out");

  File base = new File(getServletContext().getRealPath("/" + request.getServletPath()));
  String path = base.getParent();

  String sourceDir = path + "/raw/";		// savePath for formBean, sourceDir for marcBean
  if(!(new File(sourceDir)).exists()) {
      if(!(new File(sourceDir)).mkdir()) {
          throw new Exception("No raw directory to upload files to.");
      }
  }
  
  String propertiesPath = path + "/";		// path to marc.properties

  

  formBean.setSavePath(sourceDir);			// set save directory for uploaded file to "source" dir for marcBean
  formBean.setCharset("ISO8859_1");
  formBean.processForm(request, out, extensions);	// process form input
  LinkedList fileNames = formBean.getFilenames();		// get uploaded filename
  Iterator fileItr = fileNames.iterator();
  if(fileNames.size() > 0) {
    while(fileItr.hasNext()) {
      String fileName = (String)fileItr.next();
      File uploadedFile = new File(sourceDir, fileName);	// file reference to uploaded file

      if(fileName != null) {
        /* process uploaded marcfile */
        String[] outFiles = marcBean.process(saveDir, sourceDir, fileName, propertiesPath, out);
        uploadedFile.delete();	// delete uploaded marc file after use
        if(outFiles.length > 0) {
%>
<h1>Successfully processed <% out.println(fileName); %></h1>
<h2>Created files:</h2>
<ul>
<%
		String dirname = fileName.substring(
			0, fileName.indexOf(
				MarcProperties.getProperty(MarcProperties.COMMON_MARC_EXT)
			)
		);

		for(int i=0; i < outFiles.length; i++)
		{
			out.print("<li>" + outFiles[i] );
			try
			{
				File f = new File(saveDir + "/" + dirname ,outFiles[i]);
				if ( f.exists() && f.isFile() && f.length() == 0L )
				{
					boolean removed = f.delete();
					if ( removed )
					{
						out.print(": deleted empty file");
					}
					else
					{
						out.print(": unable to delete empty file: " + f.getAbsolutePath());
					}
				}
			}
			catch ( Exception ex )
			{
				out.print(": Error: " + ex.toString());
			}
			out.println("</li>");
		}
%>
</ul>
<%
        }
%>

<p>You can access these files through a web interface at: <a href="<% out.println(webPath); %>"><% out.println(webPath); %></a></p>

<%
      } else {
%>
<span class="error">Error: no files to process.</span>
<%
      }
    } // end while
  } else {
    out.println("No files to process.");
  } // end if size > 0
}
%>
</div>
<div>
<a href="uploadmarc.html">Upload another MARC File</a> 
</div>
</body>
</html>
