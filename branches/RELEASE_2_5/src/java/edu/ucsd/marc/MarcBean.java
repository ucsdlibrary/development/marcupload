package edu.ucsd.marc;

import javax.servlet.jsp.JspWriter;
// import java.lang.StackTraceElement;
/*
 * MarcBean
 * Bean interface to Marc file processer
 *
 * basically just used to catch all exceptions and print them nicely
 */

public class MarcBean{
  // properties
  public static final String CAMPUSES = "CAMPUSES";
  public static final String DESTDIRECTORY = "DESTDIRECTORY";

  // public vars
  public static final String SEPARATE = "s";
  public static final String PROPERTY_FILE = "p";
  public static final String DEFAULT_PROPERTY_FILE_NAME = "marc.properties";	// not used
  public static final String OPTIONS = SEPARATE;


  //StackTraceElement s;

  public String[] process(String saveDir, String sourceDir, String filename, String propertiesPath, JspWriter out) throws java.io.IOException {
    ProcessMarc p = new ProcessMarc();
    String[] ret = new String[0];	// empty array
    try {
      ret = p.process(saveDir, sourceDir, filename, propertiesPath, out);
    } catch (Exception e) {
      out.println("An error occured: " + e);
      
      StackTraceElement[] trace = e.getStackTrace();
      for(int i=0; i<trace.length; i++) {
		StackTraceElement s = trace[i];
        out.println(s.toString() + "<br />");
      }
      
    }

    return ret;		// return if processing is successful
  }
}
